#!/usr/bin/env python3
# -*- coding: utf-8 -*-


from TPPOO import Point


class Polygone:
    def __init__(self,sommets):
        self.sommets=sommets
        
    def getSommet(self,i):
        return self.sommets[i]
    
    def aire(self):
        S=0
        for i in range(len(self.sommets)-1):
           S = S + (self.sommets[i].x() + self.sommets[i+1].x()) * (self.sommets[i].y() + self.sommets[i+1].y())
        S = 0.5*S
        return S

    def __str__(self):
        exp="["
        for i in range(len(self.sommets)):
            exp+=str(self.sommets[i]) + ","
        return exp + ']'
    
f=Point(1,2)
g=Point(2,4)
k=Point(3,6)
l=Point(2,5)
P=Polygone([f,g,k])
#print(P.getSommet(0))
print(P.aire())
#print(P)
print(P.__str__())


class Triangle(Polygone):
    def __init__(self,a,b,c):
        super().__init__([a, b, c])
        
T=Triangle(f,g,k)
print(T)


class Rectangle(Polygone):
    def __init__(self,xMin,xMax,yMin,yMax):
        super().__init__([xMin, xMax, yMin, yMax])
       
R=Rectangle(f,g,k,l)
print(R)

        
class PolygoneRegulier(Polygone):
    def __init__(centre,   rayon,   nombreSommets):
        super().__init__([])




        
        
        
    
    















